package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"sync"
)

func simpleExec(cmdname string, args ...string) {
	cmd := exec.Command(cmdname, args...)

	// create a pipe for the output of the script
	cmdOut, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		return
	}
	cmdErr, err := cmd.StderrPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StderrPipe for Cmd", err)
		return
	}

	outscanner := bufio.NewScanner(cmdOut)
	go func() {
		for outscanner.Scan() {
			fmt.Fprintln(os.Stdout, outscanner.Text())
		}
	}()

	errscanner := bufio.NewScanner(cmdErr)
	go func() {
		for errscanner.Scan() {
			fmt.Fprintln(os.Stderr, errscanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		return
	}

	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		return
	}
}

func ioExec(w io.Writer, cmdname string, args ...string) {
	cmd := exec.Command(cmdname, args...)

	var mu sync.Mutex

	// create a pipe for the output of the script
	cmdOut, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		return
	}
	cmdErr, err := cmd.StderrPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StderrPipe for Cmd", err)
		return
	}

	outscanner := bufio.NewScanner(cmdOut)
	go func() {
		for outscanner.Scan() {
			mu.Lock()
			fmt.Fprintf(w, "StdOut: %s\n", outscanner.Text())
			mu.Unlock()
		}
	}()

	errscanner := bufio.NewScanner(cmdErr)
	go func() {
		for errscanner.Scan() {
			mu.Lock()
			fmt.Fprintf(w, "StdErr: %s\n", errscanner.Text())
			mu.Unlock()
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		return
	}

	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		return
	}
}
