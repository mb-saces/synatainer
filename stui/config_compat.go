package main

import (
	"errors"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/mb-saces/synatainer/stui/confutils"
)

// compatibility loader for config files prior 0.7 synatainer.conf
const synatainerConfEnvName = "SYNATAINER_CONFIGFILE"
const synatainerConfDefaultName = "/conf/synatainer.conf"

func loadConfigCompatSynatainerConf() error {
	// check command line
	var found bool = false
	var configName string

	for i, val := range os.Args {
		if val == "-c" || val == "config" {
			if found {
				return errors.New("duplicate config option")
			}
			found = true
			if len(os.Args) < i+2 {
				return errors.New("missing config filename parameter")
			}
			configName = os.Args[i+1]
		}
	}

	if configName == "" {
		// not set on cmd line, look up env
		val, ok := os.LookupEnv(synatainerConfEnvName)
		if ok {
			configName = val
		}
	}
	if configName == "" {
		// not set on cmd line or env, look up default file
		if confutils.FileExists(synatainerConfDefaultName) {
			configName = synatainerConfDefaultName
		}
	}
	if configName != "" {
		err := godotenv.Load(configName)
		if err != nil {
			return err
		}
	}
	return nil
}
