package mxutils

import (
	"testing"
)

func unEmailTest(t *testing.T, input string, expected string) {
	res := UnEmailHandle(input)
	if res != expected {
		t.Fatalf(`unEmailHandle("%s") returned "%s", expected "%s"`, input, res, expected)
	}
}

func TestEmpty(t *testing.T) {
	unEmailTest(t, "", "")
}

func TestText(t *testing.T) {
	unEmailTest(t, "arbitrarytext", "arbitrarytext")
}

func Test01(t *testing.T) {
	unEmailTest(t, "@user:example.com", "@user:example.com")
}

func Test02(t *testing.T) {
	unEmailTest(t, "user@example.com", "@user:example.com")
}

func Test02a(t *testing.T) {
	unEmailTest(t, "user@", "@user:")
}

func Test03(t *testing.T) {
	unEmailTest(t, "#roomalias@example.com", "#roomalias:example.com")
}

func Test04(t *testing.T) {
	unEmailTest(t, "#roomalias:example.com", "#roomalias:example.com")
}

func Test05(t *testing.T) {
	unEmailTest(t, "!roomid@example.com", "!roomid:example.com")
}

func Test06(t *testing.T) {
	unEmailTest(t, "!roomid:example.com", "!roomid:example.com")
}
