package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

func onNotify(c *pgconn.PgConn, n *pgconn.Notice) {
	fmt.Printf("%s\n", n.Message)
	if len(n.Detail) > 0 {
		fmt.Printf("%s\n", n.Detail)
	}
}

func vacuum_db(full bool, quiet bool) {
	fmt.Printf("Vacuuming database: full=%t quiet=%t\n", full, quiet)
	dburl := getPostgresConnectString()
	conf, err := pgx.ParseConfig(dburl)

	if err != nil {
		fmt.Printf("Error creating database config: %v\n", err)
		return
	}

	conf.OnNotice = onNotify

	conn, err := pgx.ConnectConfig(context.Background(), conf)
	if err != nil {
		fmt.Printf("Unable to connect to database: %v\n", err)
		return
	}
	defer conn.Close(context.Background())

	var sb strings.Builder
	sb.WriteString("VACUUM")
	if full {
		sb.WriteString(" FULL")
	}
	if !quiet {
		sb.WriteString(" VERBOSE")
	}

	_, err = conn.Exec(context.Background(), sb.String())

	if err != nil {
		fmt.Printf("Error while vacuum: %v\n", err)
		return
	}
	fmt.Println("Vacuuming database done.")
}
