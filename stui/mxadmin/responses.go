package mxadmin

import "maunium.net/go/mautrix/id"

type RespRoomDetail struct {
	RoomID               id.RoomID `json:"room_id"`
	Name                 string    `json:"name"`
	Avatar               string    `json:"avatar"`
	Topic                string    `json:"topic"`
	CanonicalAlias       string    `json:"canoncial_alias"`
	Joined_members       int       `json:"joined_members"`
	Joined_local_members int       `json:"joined_local_members"`
	Joined_local_devices int       `json:"joined_local_devices"`
	Version              string    `json:"version"`
	Creator              string    `json:"creator"`
	Encryption           string    `json:"encryption"`
	Federatable          bool      `json:"federatable"`
	Public               bool      `json:"public"`
	Join_rules           string    `json:"join_rules"`
	Guest_access         string    `json:"guest_access"`
	History_visibility   string    `json:"history_visibility"`
	State_events         int       `json:"state_events"`
}

type RespRoomMembers struct {
	Members []id.UserID `json:"members"`
	Total   int         `json:"total"`
}

type RespRoomList struct {
	Rooms      []RespRoomDetail `json:"rooms"`
	Offset     int              `json:"offset"`
	TotalRooms int              `json:"total_rooms"`
	NextBatch  int              `json:"next_batch"`
	PrevBatch  int              `json:"prev_batch"`
}

type RespRoomForwardExtremityDetail struct {
	EventID    string `json:"event_id"`
	StateGroup int64  `json:"state_group"`
	Depth      int64  `json:"depth"`
	ReceivedTS int64  `json:"received_ts"`
}

type RespRoomForwardExtremities struct {
	Count   int                              `json:"count"`
	Results []RespRoomForwardExtremityDetail `json:"results"`
}
