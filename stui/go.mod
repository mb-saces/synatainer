module gitlab.com/mb-saces/synatainer/stui

go 1.22

require (
	github.com/jackc/pgx/v5 v5.7.1
	github.com/joho/godotenv v1.4.0
	github.com/rivo/tview v0.0.0-20220307222120-9994674d60a8
	github.com/urfave/cli/v3 v3.0.0-alpha9.2
	maunium.net/go/mautrix v0.11.0
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.5.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/crypto v0.27.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	golang.org/x/term v0.24.0 // indirect
	golang.org/x/text v0.18.0 // indirect
)
