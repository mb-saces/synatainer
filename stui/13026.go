package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/jackc/pgx/v5"
	"gitlab.com/mb-saces/synatainer/stui/mxadmin"
	"maunium.net/go/mautrix/id"
)

func issue13026(rooms []string, fixattempt bool) {
	fmt.Print("Testing if you are affected by #13026. Fix is ")
	if fixattempt {
		fmt.Print("en")
	} else {
		fmt.Print("dis")
	}
	fmt.Println("abled. (If this list is empty, you are happy.)")

	dburl := getPostgresConnectString()
	conn, err := pgx.Connect(context.Background(), dburl)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	acli, err := MXAdminLogin()

	if err != nil {
		log.Fatalf("Failed to login: %v\n", err)
		return
	}

	if len(rooms) == 0 {
		//fmt.Println("All rooms.")
		listAllRooms(acli, func(rd *mxadmin.RespRoomDetail) {
			issue13026room(conn, acli, rd.RoomID, fixattempt)
		})
	} else {
		for _, rid := range rooms {
			issue13026room(conn, acli, id.RoomID(rid), fixattempt)
		}
	}
	fmt.Println("Done.")
}

func issue13026room(dbconn *pgx.Conn, acli *mxadmin.AdminClient, roomid id.RoomID, fixattempt bool) {

	resp, err := acli.RoomForwardExtremities(roomid)

	if err != nil {
		log.Fatalf("Error while cmd: %+v\n", err)
		return
	}

	var ecount int
	err = dbconn.QueryRow(context.Background(), "SELECT COUNT(*) AS count FROM events AS e JOIN event_forward_extremities efe ON e.event_id = efe.event_id WHERE e.room_id = $1;", string(roomid)).Scan(&ecount)
	if err != nil {
		log.Fatalf("Error while cmd: %+v\n", err)
		return
	}

	if resp.Count == ecount {
		return
	}

	fmt.Printf("Extra extremities in room '%s' by API = %d; by db = %d\n", string(roomid), resp.Count, ecount)
	rd, err := acli.RoomDetail(roomid)
	if err != nil {
		log.Fatalf("Error while cmd: %+v\n", err)
		return
	}

	printRoomDetails(rd)

	apiFE := make(map[string]bool, resp.Count)

	for _, fed := range resp.Results {
		apiFE[fed.EventID] = true
	}

	extraFE := make(map[string]bool, 10)

	rows, _ := dbconn.Query(context.Background(), "SELECT e.event_id FROM events AS e JOIN event_forward_extremities efe ON e.event_id = efe.event_id WHERE e.room_id = $1;", roomid)

	for rows.Next() {
		var eid string
		rows.Scan(&eid)
		if err != nil {
			log.Fatalf("Scan failed: %v\n", err)
			return
		}
		_, ok := apiFE[eid]
		if !ok {
			extraFE[eid] = true
		}
	}

	if rows.Err() != nil {
		fmt.Fprintf(os.Stderr, "Query failed: %v\n", rows.Err())
		return
	}
	rows.Close()

	for eid := range extraFE {
		fmt.Printf("Extra extremity in db: %s\n", eid)
		if fixattempt {
			_, err := dbconn.Exec(context.Background(), "DELETE FROM event_forward_extremities WHERE event_id=$1", eid)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error Deleting '%s': %v\n", eid, err)
				os.Exit(1)
			}
			fmt.Printf("Deleting %s.\n", eid)
		}
	}
	fmt.Println("Romm done.")
}
