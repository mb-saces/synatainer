package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"slices"

	"github.com/urfave/cli/v3"
)

// Make a variable for the version which will be set at build time.
var version = "unknown"

// Setup the variables you want your incoming flags to set.
var configFile string

var (
	db_user     string
	db_host     string
	db_name     string
	db_pass     string
	db_passfile string
)

var (
	mx_bearer_token string
	mx_synapse_host string
	mx_token_file   string
)

var vacuum_quiet bool
var vacuum_full bool
var sac_chunk_size string
var sac_chunks_to_compress string

// include tokens and passords in config info
var infoPrivate bool

var issue12507fixattempt bool
var issue13026fixattempt bool

func makeDbFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Sources:     cli.EnvVars("DB_USER"),
			Name:        "user",
			Destination: &db_user,
			Aliases:     []string{"u"},
			Usage:       "database user",
		},
		&cli.StringFlag{
			Sources:     cli.EnvVars("DB_HOST"),
			Name:        "host",
			Destination: &db_host,
			Aliases:     []string{"s"},
			Usage:       "database host",
		},
		&cli.StringFlag{
			Sources:     cli.EnvVars("DB_NAME"),
			Name:        "name",
			Destination: &db_name,
			Aliases:     []string{"n"},
			Usage:       "database host",
		},
		&cli.StringFlag{
			Sources:     cli.EnvVars("PGPASSWORD"),
			Name:        "password",
			Destination: &db_pass,
			Aliases:     []string{"p"},
			Usage:       "database password",
		},
		&cli.StringFlag{
			Sources:     cli.EnvVars("PGPASSFILE"),
			Name:        "pgpassfile",
			Destination: &db_passfile,
			Usage:       "pq-lib campatible pasword file",
		},
	}
}

func makeMatrixFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Sources:     cli.EnvVars("BEARER_TOKEN"),
			Name:        "token",
			Destination: &mx_bearer_token,
			Aliases:     []string{"t"},
			Usage:       "synapse admin token",
		},
		&cli.StringFlag{
			Sources:     cli.EnvVars("SYNAPSE_HOST"),
			Name:        "synapsehost",
			Destination: &mx_synapse_host,
			Aliases:     []string{"y"},
			Usage:       "synapse host",
		}, &cli.StringFlag{
			Sources:     cli.EnvVars("MXPASSFILE"),
			Name:        "mxtokenfile",
			Destination: &mx_token_file,
			Usage:       "matrix token file",
		},
	}
}

func main() {
	// compatibility: load old config
	// load the variables into process env, cli parser pick them up
	err := loadConfigCompatSynatainerConf()
	if err != nil {
		fmt.Printf("Error while loading compat layerz:\n%v\n", err)
		os.Exit(1)
	}

	cmd := &cli.Command{
		Name:  "stui",
		Usage: "Synatainer Terminal User Interface",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Sources:     cli.EnvVars("SYNATAINER_CONFIGFILE"),
				Name:        "config",
				Destination: &configFile,
				Aliases:     []string{"c"},
				Usage:       "config to use",
			},
		},
		Action: func(context.Context, *cli.Command) error {
			fmt.Println("NoNoNo, Mr. TUI not here. Sorry, sooon…")
			return nil
		},
		Commands: []*cli.Command{
			{
				Name:  "cron",
				Usage: "Start the cron(like) background server",
				Action: func(context.Context, *cli.Command) error {
					fmt.Println("Start the cron(like) background server. Not yet.")
					return nil
				},
			},
			{
				Name:  "cronhandler",
				Usage: "replacement for the old cron script?",
				Action: func(context.Context, *cli.Command) error {
					fmt.Println("Not yet")
					return nil
				},
			},
			{
				Name:  "info",
				Usage: "show configuration",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:        "private",
						Destination: &infoPrivate,
						Aliases:     []string{"p"},
						Usage:       "show private tokens and passwords",
					},
				},
				Action: func(context.Context, *cli.Command) error {
					show_config_info(infoPrivate)
					return nil
				},
			},
			{
				Name:  "db-shell",
				Usage: "Open a database shell, psql like",
				Flags: makeDbFlags(),
				Action: func(context.Context, *cli.Command) error {
					fmt.Println("Not yet")
					return nil
				},
			},
			{
				Name:  "db-info",
				Usage: "prints some database info/stats",
				Flags: makeDbFlags(),
				Action: func(context.Context, *cli.Command) error {
					db_info()
					return nil
				},
			},
			{
				Name:  "vacuum-db",
				Usage: "vacuum the whole database",
				Flags: append([]cli.Flag{
					&cli.BoolFlag{
						Name:        "quiet",
						Aliases:     []string{"q"},
						Destination: &vacuum_quiet,
						Usage:       "be not verbose",
					},
					&cli.BoolFlag{
						Name:        "full",
						Aliases:     []string{"f"},
						Destination: &vacuum_full,
						Usage:       "do a full vacuum",
					},
				}, makeDbFlags()...),
				Action: func(context.Context, *cli.Command) error {
					vacuum_db(vacuum_full, vacuum_quiet)
					return nil
				},
			},
			{
				Name:  "pgpassexport",
				Usage: "prints the line for the PGPASSFILE",
				Flags: makeDbFlags(),
				Action: func(context.Context, *cli.Command) error {
					print_pgpass_line()
					return nil
				},
			},
			{
				Name:  "12507",
				Usage: "tool for synapse issue #12507 (No state group for unknown or outlier event)",
				Description: `
Tests if you are affected by this issue.
Use --fix to delete the bogous things.
More info about the issue: https://github.com/matrix-org/synapse/issues/12507
`,
				Flags: append([]cli.Flag{
					&cli.BoolFlag{
						Name:        "fix",
						Aliases:     []string{"f"},
						Destination: &issue12507fixattempt,
						Usage:       "delete bogous things",
					},
				}, makeDbFlags()...),
				Action: func(context.Context, *cli.Command) error {
					issue12507(issue12507fixattempt)
					return nil
				},
			},
			{
				Name:  "room",
				Usage: "show room details",
				Flags: makeMatrixFlags(),
				Action: func(_ context.Context, cmd *cli.Command) error {
					doRoomCmd(cmd.Args().Get(0), cmd.Args().Get(1))
					return nil
				},
			},
			{
				Name:  "rooms",
				Usage: "list rooms",
				Flags: makeMatrixFlags(),
				Action: func(_ context.Context, cmd *cli.Command) error {
					doRoomsCmd(cmd.Args().Get(0))
					return nil
				},
			},
			{
				Name:  "autocompressor",
				Usage: "run the synapse_auto_compressor",
				Flags: append([]cli.Flag{
					&cli.StringFlag{
						Sources:     cli.EnvVars("STATE_AUTOCOMPRESSOR_CHUNK_SIZE"),
						Name:        "chunksize",
						Value:       "500",
						Destination: &sac_chunk_size,
						Usage:       "chunk size",
					},
					&cli.StringFlag{
						Sources:     cli.EnvVars("STATE_AUTOCOMPRESSOR_CHUNKS_TO_COMPRESS"),
						Name:        "chunkstocompress",
						Value:       "100",
						Destination: &sac_chunks_to_compress,
						Usage:       "chunks to compress",
					},
				}, makeDbFlags()...),
				Action: func(context.Context, *cli.Command) error {
					autocompressor()
					return nil
				},
			},
			{
				Name:  "13026",
				Usage: "tool for synapse issue #13026 (No state group for unknown or outlier event)",
				Description: `
Tests if you are affected by this issue.
Use --fix to delete the bogous things.
More info about the issue: https://github.com/matrix-org/synapse/issues/13026
`,
				Flags: slices.Concat([]cli.Flag{
					&cli.BoolFlag{
						Name:        "fix",
						Aliases:     []string{"f"},
						Destination: &issue13026fixattempt,
						Usage:       "delete bogous things",
					},
				}, makeDbFlags(), makeMatrixFlags()),
				Action: func(_ context.Context, cmd *cli.Command) error {
					issue13026(cmd.Args().Slice(), issue13026fixattempt)
					return nil
				},
			},
			{
				Name:  "mxpassexport",
				Usage: "prints the line for the MXPASSFILE",
				Flags: makeMatrixFlags(),
				Action: func(context.Context, *cli.Command) error {
					print_mxpass_line()
					return nil
				},
			},
			{
				Name:  "mxtoken",
				Usage: "prints the matrix token for shell scripts",
				Action: func(context.Context, *cli.Command) error {
					print_mxtoken()
					return nil
				},
			},
			{
				Name:  "pgconnectstring",
				Usage: "prints the psql connect string for shell scripts",
				Action: func(context.Context, *cli.Command) error {
					print_pg_connect_string()
					return nil
				},
			},
		},
	}

	if err := cmd.Run(context.Background(), os.Args); err != nil {
		log.Fatal(err)
	}
}
