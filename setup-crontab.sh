#!/bin/sh

# if i figure out the syntax this may go inlined in entrypoint

set -eu -o pipefail

CONFIG_FILE=/conf/synatainer.conf && test -f $CONFIG_FILE && source $CONFIG_FILE

if [ -f "/conf/synatainer.crontab" ]; then
  cat /conf/synatainer.crontab | crontab -
else
  echo "2 2 * * * /usr/local/bin/synatainer-cron.sh" | crontab -
fi

if [ -n "${MAILTO:-}" ]; then
  (echo "MAILTO=$MAILTO"; crontab -l) | crontab -
fi

crontab -l
